package com.ncr.chess;

import static com.ncr.chess.MovementType.MOVE;
import static com.ncr.chess.PieceColor.BLACK;

public class Pawn {

    private ChessBoard chessBoard;
    private int xCoordinate;
    private int yCoordinate;
    private final PieceColor pieceColor;

    public Pawn(PieceColor pieceColor) {
        this.pieceColor = pieceColor;
    }

    public ChessBoard getChessBoard() {
        return chessBoard;
    }

    public void setChessBoard(ChessBoard chessBoard) {
        this.chessBoard = chessBoard;
    }

    public int getXCoordinate() {
        return xCoordinate;
    }

    public void setXCoordinate(int value) {
        this.xCoordinate = value;
    }

    public int getYCoordinate() {
        return yCoordinate;
    }

    public void setYCoordinate(int value) {
        this.yCoordinate = value;
    }

    public PieceColor getPieceColor() {
        return this.pieceColor;
    }

    public void move(MovementType movementType, int newX, int newY) {
    	if (!chessBoard.isLegalBoardPosition(newX, newY)) {
    		return;
    	}
    	if (MOVE == movementType) { 
    		if (isValidateMove(newX, newY)) {
    			this.setYCoordinate(newY);
    		}
    	}
    }

	private boolean isValidateMove( int newX, int newY) {
    		if (this.xCoordinate != newX) {
    			return false;
    		} 
    		
    		if (this.xCoordinate == newX && this.yCoordinate == newY) {
    			return false;
    		}
    		
    		if (this.getPieceColor() == BLACK) {
    			if (this.yCoordinate != newY + 1) {
    				return false;
    			}
        	} else {
        		if (this.yCoordinate != newY - 1) {
    				return false;
    			}
        	}
    		return true;
	}

    @Override
    public String toString() {
        return getCurrentPositionAsString();
    }

    protected String getCurrentPositionAsString() {
        final String eol = System.lineSeparator();
        return String.format("Current X: {1}{0}Current Y: {2}{0}Piece Color: {3}", eol, xCoordinate, yCoordinate, pieceColor);
    }
}
