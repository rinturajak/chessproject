package com.ncr.chess;

public class ChessBoard {

	public static int MAX_BOARD_WIDTH = 7;
	public static int MAX_BOARD_HEIGHT = 7;

	private final Pawn[][] pieces;

	public ChessBoard() {
		pieces = new Pawn[MAX_BOARD_WIDTH][MAX_BOARD_HEIGHT];
	}

	public void addPiece(Pawn pawn, int xCoordinate, int yCoordinate, PieceColor pieceColor) {
		if (isLegalBoardPosition(xCoordinate, yCoordinate)) {
			if (isLegalPawnPosition(xCoordinate, yCoordinate)) {
				pawn.setXCoordinate(xCoordinate);
				pawn.setYCoordinate(yCoordinate);
				pieces[xCoordinate][yCoordinate] = pawn;
			} else {
				pawn.setXCoordinate(-1);
				pawn.setYCoordinate(-1);
			}
		}
	}

	public boolean isLegalBoardPosition(int xCoordinate, int yCoordinate) {
		if (xCoordinate >= 0 && yCoordinate >= 0 && xCoordinate <= MAX_BOARD_WIDTH && yCoordinate <= MAX_BOARD_HEIGHT) {
			return true;
		}
		return false;
	}
	
	public boolean isLegalPawnPosition(int xCoordinate, int yCoordinate) {
		if (xCoordinate < 0 || xCoordinate >= MAX_BOARD_WIDTH || yCoordinate < 0 || yCoordinate >= MAX_BOARD_HEIGHT) {
			return false;
		} else if (pieces[xCoordinate][yCoordinate] != null) {
			return false;
		}
		return true;
	}
	
	
	
}
